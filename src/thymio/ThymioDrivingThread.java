package thymio;

import context.Coordinate;
import context.Map;
import observer.MapPanel;

import java.util.ArrayList;

public class ThymioDrivingThread extends Thread {
    private Thymio myThymio;
    private int i = 1;
    private ArrayList<Coordinate> shortestPath;
    private Map map;
    private double angle;

    public ThymioDrivingThread(Thymio t) {
        myThymio = t;
    }

    public void run() {
        while (true) {

            shortestPath = myThymio.getMap().getShortestPath();
            map = myThymio.getMap();
            MapPanel mapPanel = myThymio.getMyPanel();
            //calculating angle from actual field to next field
            angle = Math.toDegrees(Math.atan2(shortestPath.get(i).getY() - mapPanel.getEstimPosY(), shortestPath.get(i).getX() - mapPanel.getEstimPosY()));

//        synchronized (myThymio) {
//            try {
//                myThymio.wait();
//                myThymio.setVLeft((short) (v - angle));
//                myThymio.setVRight((short) (v + angle));
            rotateAndDrive(shortestPath, map, angle);

            if (shortestPath.get(i).getY() == map.getThymioY() && shortestPath.get(i).getX() == map.getThymioX()) {
                myThymio.setVLeft((short) 0);
                myThymio.setVRight((short) 0);
                i++;
                System.out.println(" wechesel  " + "thymX " + map.getThymioX() + " thymY " + map.getThymioY() + " i " + i);
                angle = Math.toDegrees(Math.atan2(shortestPath.get(i).getY() - mapPanel.getEstimPosY(), shortestPath.get(i).getX() - mapPanel.getEstimPosY()));
                rotateAndDrive(shortestPath, map, angle);
            }
//            } catch (InterruptedException e) {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            }
        }
    }

    private void rotateAndDrive(ArrayList<Coordinate> shortestPath, Map map, double angle) {
        while (shortestPath.get(i).getY() != map.getThymioY() && shortestPath.get(i).getX() != map.getThymioX()) {
            while (myThymio.getOdomRotation() < angle) {
                if (myThymio.getOdomRotation() < angle - 2) {
                    myThymio.setVLeft((short) 0);
                    myThymio.setVRight((short) 200);
                    myThymio.updatePose(System.currentTimeMillis());
                    System.out.println("thymX " + map.getThymioX() + " thymY " + map.getThymioY() + " i " + i);
                } else if (myThymio.getOdomRotation() < angle && myThymio.getOdomRotation() > angle -1) {
                    driveStraight();
                    myThymio.updatePose(System.currentTimeMillis());
                } else {
                    myThymio.setVLeft((short) 0);
                    myThymio.setVRight((short) 30);
                    myThymio.updatePose(System.currentTimeMillis());
                    System.out.println("thymX " + map.getThymioX() + " thymY " + map.getThymioY() + " i " + i);
                }
            }
            while (myThymio.getOdomRotation() > angle) {
                if (myThymio.getOdomRotation() > angle + 2) {
                    myThymio.setVLeft((short) 200);
                    myThymio.setVRight((short) 0);
                    myThymio.updatePose(System.currentTimeMillis());
                    System.out.println("thymX " + map.getThymioX() + " thymY " + map.getThymioY() + " i " + i);
                } else if (myThymio.getOdomRotation() > angle && myThymio.getOdomRotation() < angle +1) {
                    driveStraight();
                    myThymio.updatePose(System.currentTimeMillis());
                }else {
                    myThymio.setVLeft((short) 30);
                    myThymio.setVRight((short) 0);
                    myThymio.updatePose(System.currentTimeMillis());
                    System.out.println("thymX " + map.getThymioX() + " thymY " + map.getThymioY() + " i " + i);
                }
            }
        }
    }

    private void stopThymio() {
        myThymio.setVLeft((short) 0);
        myThymio.setVRight((short) 0);
    }

    private void driveStraight(){
        myThymio.setVLeft((short) 100);
        myThymio.setVRight((short) 100);
    }
//	}
}
