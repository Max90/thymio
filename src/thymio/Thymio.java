package thymio;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import context.Map;
import observer.MapPanel;
import observer.ThymioInterface;

public class Thymio {
	private short vleft;
	private short vright;
	private ThymioInterface myInterface;
	private ThymioDrivingThread myControlThread;
	private ThymioClient myClient;
	private long lastTimeStamp;
	private MapPanel myPanel;
	private PrintWriter logData;

    private double odomRotation;
    private Map map;
    private double actualRotation;
	
	public static final double MAXSPEED = 500;
	public static final double SPEEDCOEFF = 2.93;
	public static final double BASE_WIDTH = 95;
	public Thymio(MapPanel p) {
		vleft = vright = 0;
		
		myPanel = p;
        map = p.getMap();
		myClient = new ThymioClient();
		myInterface = new ThymioInterface(this);
		myControlThread = new ThymioDrivingThread(this);
		myControlThread.start();
		lastTimeStamp = Long.MIN_VALUE;
		
		setVLeft((short)0);
		setVRight((short)0);
		
		try {
			logData = new PrintWriter(new FileWriter("./logdata.csv"));
			logData.println("motor.left.speed\tmotor.right.speed\tdelta x observed\tdelta x computed\tdelta theta observed\tdelta theta computed\tpos X\tposY\tvertical 0\tvertical 1");
			logData.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public ThymioInterface getInterface() {
		return myInterface;
	}
	
	public int getVLeft() {
		return vleft;
	}
	
	public synchronized void setVLeft(short v) {
		ArrayList<Short> data = new ArrayList<Short>();
		this.vleft = v;
		
		data.add(new Short(v));
		myClient.setVariable("motor.left.target", data);
	}

	public synchronized void setVRight(short v) {
		ArrayList<Short> data = new ArrayList<Short>();
		this.vright = v;
		
		data.add(new Short(v));
		myClient.setVariable("motor.right.target", data);
	}

	public int getVRight() {
		return vright;
	}

	public synchronized void updatePose(long now) {
		List<Short> sensorData;
		if (lastTimeStamp > Long.MIN_VALUE) {
			long dt = now - lastTimeStamp;
			double secsElapsed = ((double)dt)/1000.0;
			double distForward; // distance passed in secsElpased in forward direction of the robot
			double distRotation; // angle covered in secsElapsed around Thymio's center
			short odomLeft = Short.MIN_VALUE, odomRight = Short.MIN_VALUE;
			double odomForward;

			sensorData = myClient.getVariable("motor.left.speed");
			if (sensorData != null) odomLeft = sensorData.get(0);
			else System.out.println("no data for motor.left.speed");
			sensorData = myClient.getVariable("motor.right.speed");
			if (sensorData != null) odomRight = sensorData.get(0);
			else System.out.println("no data for motor.right.speed");

			sensorData = myClient.getVariable("prox.ground.delta");
			
			if (odomLeft == Short.MIN_VALUE || odomRight == Short.MIN_VALUE) return;
			else logData.print(odomLeft + "\t" + odomRight + "\t");

			odomForward = secsElapsed*(odomLeft+odomRight)/(2.0*10.0*SPEEDCOEFF); // estimated distance in cm travelled is secsElapsed seconds.
			odomRotation = Math.atan2(odomRight-odomLeft, BASE_WIDTH);

			distForward = myInterface.getVForward()*secsElapsed;
			distRotation = Math.PI/180*myInterface.getOrientation()*secsElapsed;

			logData.print(odomForward + "\t" + distForward + "\t" + odomRotation + "\t" + distRotation + "\t");

//			if (distForward == 0 && distRotation == 0) myPanel.updatePose(distForward, distRotation, secsElapsed);
			 myPanel.updatePose(odomForward, odomRotation, secsElapsed);

			logData.print(myPanel.getEstimPosX() + "\t" +myPanel.getEstimPosY() + "\t");
			logData.println(sensorData.get(0) + "\t" + sensorData.get(1)); 
			logData.flush();

            actualRotation += odomRotation;
            System.out.println(actualRotation);
		}
		lastTimeStamp = now;

		notify();
	}

    public double getOdomRotation() {
        return actualRotation;
    }

    public Map getMap() {
        return map;
    }

    public MapPanel getMyPanel() {
        return myPanel;
    }
}
