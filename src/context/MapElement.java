package context;

public class MapElement {
	private int posX;				// position in the map
	private int posY;
	
	private boolean occupied; 		// element is known in advance to be occupied
	private double probOccupied;	// estimation of the state of this element
									// from observations
	
	private boolean onBeam;			// set temporarily if the element is hit by infrared beam
	
	private int fValue;
	private int gValue;
	private int hValue;
	
	private MapElement parent;
	
	public int getPosX() {
		return posX;
	}
	
	public int getPosY() {
		return posY;
	}
	
	public boolean isOccupied() {
		return occupied;
	}
	
	public double getProbOccupied() {
		return probOccupied;
	}
	
	public MapElement(int posX, int posY) {
		super();
		this.posX = posX;
		this.posY = posY;
	}
	
	public MapElement(int posX, int posY, boolean occupied) {
		super();
		this.posX = posX;
		this.posY = posY;
		this.occupied = occupied;
	}
	
	public void setOccupied() {
		occupied = true;
	}

	public boolean onBeam() {
		return onBeam;
	}

	public void setOnBeam(boolean onBeam) {
		this.onBeam = onBeam;
	}

	public int getFValue() {
		return fValue;
	}

	public void setFValue(int fValue) {
		this.fValue = fValue;
	}

	public int getGValue() {
		return gValue;
	}

	public void setGValue(int gValue) {
		this.gValue = gValue;
	}

	public int getHValue() {
		return hValue;
	}

	public void setHValue(int hValue) {
		this.hValue = hValue;
	}

	public MapElement getParent() {
		return parent;
	}

	public void setParent(MapElement parent) {
		this.parent = parent;
	}
	
}
