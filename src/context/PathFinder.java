package context;

import java.util.ArrayList;

public class PathFinder {

    private MapElement[][] elements;
    private ArrayList<MapElement> closedList = new ArrayList<MapElement>();
    private ArrayList<MapElement> openList = new ArrayList<MapElement>();

    private int smallestFValue = Integer.MAX_VALUE;
    private MapElement smallestFValueElement;

    public PathFinder(MapElement[][] elements) {
        this.elements = elements;
    }

    public ArrayList<Coordinate> getShortestPath() {
        closedList.clear();
        openList.clear();

        MapElement startNode = elements[0][0];
        MapElement endNode = elements[elements.length - 1][elements[0].length - 1];

        openList.add(startNode);
        smallestFValueElement = startNode;

        MapElement current = startNode;

        ArrayList<MapElement> neighbours = new ArrayList<MapElement>();

        while (current != endNode) {
            current = smallestFValueElement;
            openList.remove(current);
            closedList.add(current);
            smallestFValue = Integer.MAX_VALUE;

            if (current.equals(endNode)) {
                endNode.setParent(current.getParent());
                break;
            }

            neighbours = getWalkableNeighbours(current);

            for (int i = 0; i < neighbours.size(); i++) {
                if (!closedList.contains(neighbours.get(i))) {
                    int gValue = 0;
                    if (neighbours.get(i).getPosX() != current.getPosX()
                            && neighbours.get(i).getPosY() != current.getPosY()) {
                        gValue = current.getGValue() + 14;
                    } else {
                        gValue = current.getGValue() + 10;
                    }
                    int hValue = getManhattanDistance(neighbours.get(i), endNode);
                    int fValue = gValue + hValue;

                    if (openList.contains(neighbours.get(i))) {
                        if (neighbours.get(i).getGValue() > gValue) {
                            neighbours.get(i).setGValue(gValue);
                            neighbours.get(i).setFValue(fValue);
                            neighbours.get(i).setParent(current);
                        }
                    } else {
                        openList.add(neighbours.get(i));
                        neighbours.get(i).setGValue(gValue);
                        neighbours.get(i).setFValue(fValue);
                        neighbours.get(i).setParent(current);
                    }
                }
            }
            setSmallestElement();
        }
        ArrayList<Coordinate> path = new ArrayList<Coordinate>();
        MapElement pathNode = endNode;
        path.add(0, new Coordinate(pathNode.getPosX(), pathNode.getPosY()));
        while (pathNode != startNode) {
            pathNode = pathNode.getParent();
            path.add(0, new Coordinate(pathNode.getPosX(), pathNode.getPosY()));
        }
        return path;
    }

    private void setSmallestElement() {
        for (int i = 0; i < openList.size(); i++) {
            if (openList.get(i).getFValue() < smallestFValue) {
                smallestFValue = openList.get(i).getFValue();
                smallestFValueElement = openList.get(i);
            }
        }
    }

    private ArrayList<MapElement> getWalkableNeighbours(MapElement element) {
        int x = element.getPosX();
        int y = element.getPosY();
        ArrayList<MapElement> neighbours = new ArrayList<MapElement>();
        for (int i = -1; i <= 1; i++) {
            for (int j = -1; j <= 1; j++) {
                if (x + i > 0 && x + i < elements.length && y + j > 0
                        && y + j < elements[0].length && !(i == 0 && j == 0)) {
                    if (!elements[x + i][y + j].isOccupied()) {
                        //nur diagonal falls angrenzende Felder frei da sonst Kollision
                        if (i != 0
                                && j != 0
                                && (elements[x + i][y].isOccupied() || elements[x][y + j]
                                .isOccupied())) {
                            continue;
                        }
                        neighbours.add(elements[x + i][y + j]);
                    }
                }
            }
        }
        return neighbours;
    }

    private int getManhattanDistance(MapElement current, MapElement end) {
        return (Math.abs(end.getPosX() - current.getPosX()) + Math.abs(end
                .getPosY() - current.getPosY())) * 10;
    }
}
